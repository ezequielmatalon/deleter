package com.thomsonreuters.deleter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Execution {
	public static File directory;

	public static void main(String[] args) throws IOException {
		String path = args[0];
		Integer daysBefore = Integer.valueOf(args[1]);
		DateTime date = new DateTime().minusDays(daysBefore);
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
		directory = new File(path);
		IOFileFilter ioff = new IOFileFilter() {

			public boolean accept(File dir, String name) {
				return false;
			}

			public boolean accept(File file) {
				return file.isDirectory();
			}
		};
		Collection<File> files = FileUtils.listFilesAndDirs(directory, ioff,
				ioff);
		files.remove(directory);
		for (File file : files) {

			DateTime dirDate = null;
			try {
				dirDate = format.parseDateTime((file.getName()));
			} catch (Exception e) {
				dirDate = new DateTime();
			}
			if (date.isAfter(dirDate)) {
				FileUtils.deleteDirectory(file);
			}

		}
	}
}
